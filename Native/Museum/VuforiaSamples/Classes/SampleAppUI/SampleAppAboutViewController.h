/*===============================================================================
 Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.
 
 Vuforia is a trademark of PTC Inc., registered in the United States and other
 countries.
 ===============================================================================*/

#import <UIKit/UIKit.h>
#import "AltBeacon.h"

@interface SampleAppAboutViewController : UIViewController <AltBeaconDelegate>
@property (weak, nonatomic) IBOutlet UITextView *uiTextView;


@end
