/*===============================================================================
 Copyright (c) 2012-2015 Qualcomm Connected Experiences, Inc. All Rights Reserved.
 
 Vuforia is a trademark of PTC Inc., registered in the United States and other
 countries.
 ===============================================================================*/

#import "SampleAppAboutViewController.h"

@interface SampleAppAboutViewController ()

@property (strong, nonatomic) AltBeacon* beacon1;
@property (strong, nonatomic) AltBeacon* beacon2;
@property (strong, nonatomic) AltBeacon* beacon3;

@end

@implementation SampleAppAboutViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(dismissAppController:)
                                                 name:@"kDismissAppViewController"
                                               object:nil];
    
    self.uiTextView.text = @"empty beacons list";
    
    self.beacon1 = [[AltBeacon alloc] initWithIdentifier:@"37b17745-17f4-44cd-bb70-d49e55811447"];
    self.beacon2 = [[AltBeacon alloc] initWithIdentifier:@"faac8f12-5993-4ea7-96dc-a0f822373d15"];
    self.beacon3 = [[AltBeacon alloc] initWithIdentifier:@"cff16f89-b5ee-45e3-b7d7-b22a30fcc8f5"];
    
    [self.beacon1 addDelegate:self];
    [self.beacon2 addDelegate:self];
    [self.beacon3 addDelegate:self];
    
    [self.beacon1 startBroadcasting];
    [self.beacon2 startBroadcasting];
    //[self.beacon3 startBroadcasting];
    
    [self.beacon1 startDetecting];
    [self.beacon2 startDetecting];
    [self.beacon3 startDetecting];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    // We ensure the navigation bar is shown
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

// Delegate methods
- (void)service:(AltBeacon *)service foundDevices:(NSMutableDictionary *)devices {
    
    NSMutableString *beaconsText = [NSMutableString stringWithFormat:@"found beacons %d:\n", [[devices allKeys] count]];
    
    for(NSString *key in devices) {
        [beaconsText appendString:key];
        
        NSNumber * range = [devices objectForKey:key];
        if (range.intValue == INDetectorRangeUnknown){
            [beaconsText appendString:@" : 0\n"];
        }else {
            NSString *result = [self convertToString:range];
            
            [beaconsText appendString:[NSString stringWithFormat:@" : %@ :: %@", range, result]];
            
        }
    }
    
    self.uiTextView.text = beaconsText;
}

- (NSString*) convertToString:(NSNumber *)number {
    NSString *result = nil;
    
    switch(number.intValue) {
        case INDetectorRangeFar:
            result = @"Up to 100 meters";
            break;
        case INDetectorRangeNear:
            result = @"Up to 15 meters";
            break;
        case INDetectorRangeImmediate:
            result = @"Up to 5 meters";
            break;
            
        default:
            result = @"Unknown";
    }
    
    return result;
}

//------------------------------------------------------------------------------
#pragma mark - Autorotation
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL)shouldAutorotate {
    return YES;
}

//------------------------------------------------------------------------------
#pragma mark - Private

- (void) dismissAppController:(id) sender
{
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    
}

@end
